<?
require_once('php/conexao.php');
require_once('php/models/Promocoes.php');

$aResult = array();
if($_SERVER['REQUEST_METHOD'] == 'POST'){

  $aResult = Promocoes::pesquisaPromocoes($_POST['pesquisa']);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Mural de Promoções</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/jumbotron-narrow.css">
    </head>
    <body>
        <div class="container">

      <div class="jumbotron">
        <h2>Policia DIC</h2>
        <p class="lead">Pesquisa de Promoções</p>

        <form action="mural_promocoes.php" method="POST">
            <div class="form-group has-feedback">
                <label class="control-label">Policial</label>
                <input name="pesquisa" type="text" class="form-control" placeholder="Policial" />
                <i class="glyphicon glyphicon-search form-control-feedback"></i>
            </div>
        </form>
      </div>

      <?if(count($aResult) > 0):
      foreach($aResult as $sResult):?>
        <blockquote>
            <h4><span class="text-primary">Policial:</span> <?=$sResult['policial'];?></h4>
            <span class="text-primary">Promovido em:</span> <?=Utils::showDate($sResult['dtpromocao']);?><br/>
            <span class="text-primary">Patente Anterior:</span> <?=$sResult['patente_antiga'];?>
            <span class="text-primary">Patente Nova:</span> <?=$sResult['patente_nova'];?> <br/>
            <span class="text-primary">Motivo Promoção:</span> <?=$sResult['motivo_promocao']?>
        </blockquote>
      <?endforeach;?>
      <?endif;?>

      <footer class="footer">
        <p>&copy; 2015 Company, Inc.</p>
      </footer>

          </div> <!-- /container -->

          </body>
      </html>
