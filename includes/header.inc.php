<!-- top navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
   <div class="container">
    <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?=Uri::linkTo('dashboard.php')?>">Sistema</a>
       <img style="margin-top:15px;position:absolute" src="http://www.habbcrazy.net/resources/fonts/1/DIC.gif" alt="DIC" />
    </div>
    <ul class="nav navbar-nav navbar-right">
        <li><a href='#'>Usuário Logado: <?=Session::getField('nome')?></a></li>
        <li><a href="<?=Uri::linkTo('php/logout.php')?>">(Sair)</a></li>
    </ul>
   </div>
</div>
