<!-- sidebar -->
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <ul class="nav">
      <li class="active"><a href="<?=Uri::linkTo('dashboard.php')?>">Início</a></li>
      <?if(Session::userIsAdmin()):?>
        <li><a href="<?=Uri::linkTo('policiais/pl_consulta.php')?>">Policiais</a></li>
      <?endif;?>
      <li><a href="<?=Uri::linkTo('promocoes/pr_consulta.php')?>">Promoções</a></li>
      <li><a target="_blank" href="<?=Uri::linkTo('mural_promocoes.php')?>">Mural de Promoções</a></li>
    </ul>
</div>
