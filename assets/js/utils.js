
function fillFormData(aData){

  var jsonData = JSON.parse(aData);
  for(var formField in jsonData) {
      var formValue = jsonData[formField];
      $('input[type="text"][name="'+formField+'"], input[type="hidden"][name="'+formField+'"], select[name="'+formField+'"], textarea[name="'+formField+'"]').val(formValue);

      if($('input[type="radio"][name="'+formField+'"]')){
        $('input[type="radio"][name="'+formField+'"]').removeAttr('checked');
        if(formValue == "S")
            $('input[type="radio"][name="'+formField+'"][value="S"]').attr('checked', 'true');
        else
            $('input[type="radio"][name="'+formField+'"][value="N"]').attr('checked', 'true');
      }
  }
}
