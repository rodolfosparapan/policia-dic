<?
require_once('helpers/session_helper.php');
require_once('helpers/uri_helper.php');

if(Session::getField('id_usuario') == null){
    
    Uri::redirectTo('index.php');
}