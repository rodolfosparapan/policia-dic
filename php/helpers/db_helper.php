<?
class DB{

    private static $num_rows;
    private static $increment_id;

    public static function select($sSql, $aParams=''){
        global $conn;
        $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if(is_array($aParams)){

            $stmt = $conn->prepare($sSql);
            $stmt->execute($aParams);

            $aReturn = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $stmt = $conn->query($sSql);
            $aReturn = $stmt->fetchAll();
        }

        DB::$num_rows = $stmt->rowCount();
        return $aReturn;
    }

    public static function getNumRows(){
        return DB::$num_rows;
    }

    public static function update($sTable, $aDados, $aWhere){

        global $conn;
        $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $aSet = array();
        foreach($aDados as $sField => $sValue){
            $aSet[] = $sField . "=:{$sField}";
        }

        $aWhereAdd = array();
        foreach($aWhere as $sField){
            $aWhereAdd[] = $sField . "=:{$sField}";
        }

        $sSql = "UPDATE {$sTable} SET " . implode(",",$aSet) . " WHERE " . implode(" AND ", $aWhereAdd);
        $stmt = $conn->prepare($sSql);

        $aToExec = array();
        foreach($aDados as $sKey => $sValue){
            $aToExec[':'.$sKey] = $sValue;
        }

        $bSuccess = $stmt->execute($aToExec);

        if(!$bSuccess)
            DB::logError();

        return $bSuccess;
    }

    public static function getCount($sTable, $aData){

      $aWhere = array();
      foreach($aData as $sField => $sValue){
        $aWhere[] = $sField . "=?";
      }
      $sWhere = "WHERE " . implode(" AND ", $aWhere);
      $sSql = sprintf("SELECT * FROM %s %s", $sTable, $sWhere);

      $aReturn = DB::select($sSql, array_values($aData));
      return count($aReturn);
    }

    public static function delete($sTable, $aWhere){

        global $conn;
        $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $aWhereAdd = array();
        foreach($aWhere as $sField => $sValue){
            $aWhereAdd[] = $sField . "=:{$sField}";
        }

        $sSql = "DELETE FROM {$sTable} WHERE " . implode(" AND ", $aWhereAdd);
        $stmt = $conn->prepare($sSql);

        $aToExec = array();
        foreach($aWhere as $sKey => $sValue){
            $aToExec[':'.$sKey] = $sValue;
        }

        $bSuccess = $stmt->execute($aToExec);
        DB::$increment_id = $conn->lastInsertId();

        if(!$bSuccess){
            DB::logError();
        }

        return $bSuccess;
    }

    public static function insert($sTable, $aParams){

        global $conn;
        $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sSql = 'insert into ' . $sTable . '(' . implode(',', array_keys($aParams)) . ',dtcadastro)
                 values(:' . implode(',:', array_keys($aParams)) . ',now())';

        $stmt = $conn->prepare($sSql);

        $aToExec = array();
        foreach($aParams as $sKey => $sValue){
            $aToExec[':'.$sKey] = $sValue;
        }

        $bSuccess = $stmt->execute($aToExec);
        DB::$increment_id = $conn->lastInsertId();

        if(!$bSuccess){
            DB::logError();
        }

        return $bSuccess;
    }

    public static function getIncrementId(){

        return DB::$increment_id;
    }

    public static function logError(){

        global $conn;

        if(!isPublicado()){
            echo "<pre>";
            die(print_r($conn->errorInfo()));
            echo "</pre>";
        }
    }

}
