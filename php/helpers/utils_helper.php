<?
class Utils
{
    public static function showDate($sMySqlDate){
      return date("d/m/Y", strtotime($sMySqlDate));
    }

    public static function showDateTime($sMySqlDateTime){
      return date("d/m/Y H:i:s", strtotime($sMySqlDateTime));
    }
}

function debug($value){
    echo '<pre>';
    die(print_r($value));
    echo '</pre>';
}
