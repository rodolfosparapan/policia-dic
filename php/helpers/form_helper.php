<?
class FormHelper
{
    public static function getSelectOptions($aValues)
    {
        $html = "";
        foreach($aValues as $iKey => $sValue)
        {
            $html .= "<option value='" . $iKey . "'>" . $sValue . "</option>";
        }
        
        return $html;
    }
}