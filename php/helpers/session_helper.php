<?
class Session{

    private static $sSessionPrefix = 'policia_dic_';

    public static function setUserSession($aUserDados){

        Session::setField('id_usuario', $aUserDados['id_policial']);
        Session::setField('nome', $aUserDados['nome']);
        Session::setField('admin', $aUserDados['admin']);
        Session::setField('logado', Date('d/m/Y') . ' às ' . Date('H:i:s'));
    }

    public static function userIsAdmin(){
        return (Session::getField('admin') == 'S');
    }

    public static function setField($sField, $sValue){

        $_SESSION[Session::$sSessionPrefix . $sField] = $sValue;
    }

    public static function getField($sField){

        if(isset($_SESSION[Session::$sSessionPrefix . $sField])){
          return $_SESSION[Session::$sSessionPrefix . $sField];
        }
        return null;
    }

    public static function setFlashMessage($sType, $sMessage){

        Session::setField('temp_message_type', $sType);
        Session::setField('temp_message', $sMessage);
    }

    public static function unsetField($sField){

        Session::setField($sField, null);
    }

    public static function hasFlashMessage(){
        return (Session::getField('temp_message') != null ? true : false);
    }

    public static function getFlashMessage(){

        if(Session::hasFlashMessage()){

            $sMessage = Session::getField('temp_message');
            Session::unsetField('temp_message');
            return $sMessage;
        }

        return null;
    }

    public static function getFlashMessageType(){

      if(Session::hasFlashMessage()){

          $sType = Session::getField('temp_message_type');
          Session::unsetField('temp_message_type');
          return $sType;
        }

        return null;
    }
}
