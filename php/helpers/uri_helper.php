<?
class Uri{

    public static function getRootUri(){

        if(isPublicado()){

            return 'http://' . $_SERVER['HTTP_HOST'] . '/';
        }

        return 'http://' . $_SERVER['HTTP_HOST'] . '/policia-dic/';
    }

    public static function redirectTo($sPage){

        header('Location: ' . Uri::getRootUri() . $sPage);
        exit; // SUPER IMPORTANTE, se não chamar esse cara, o servidor perde a sessão após o redirect
    }

    public static function linkTo($sPage){

        return Uri::getRootUri() . $sPage;
    }
}
