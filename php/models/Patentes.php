<?
require_once("../php/conexao.php");
require_once("../php/helpers/db_helper.php");

class Patente{

    public $id;
    public $patente;
}

class Patentes
{
    public static function getPatentes()
    {
        $aData = DB::select("SELECT * FROM patentes WHERE ativo='S'");
        $aRet = array();

        foreach($aData as $aValue)
        {
            $aRet[$aValue['id_patente']] = $aValue['patente'];
        }
        
        return $aRet;
    }
}