<?
class Promocoes{

  public static function pesquisaPromocoes($sPesquisa){

    $aSearch = explode(' ', $sPesquisa);
    $aWhere = array();
    $aParams = array();
    foreach($aSearch as $sSearch){
      $aParams[] = "%{$sSearch}%";
      $aWhere[] = "policial LIKE ?";
    }

    $sWhere = "WHERE " . implode(" OR ", $aWhere);
    return DB::select("SELECT * FROM vw_promocoes " . $sWhere . " ORDER BY policial ASC, id_promocao DESC", $aParams);
  }
}
