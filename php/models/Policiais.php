<?
class Policiais{

  public static function getPoliciais(){

    return DB::select("SELECT p.id_policial, p.usuario, p.nome, pt.patente, p.dtcadastro FROM policiais p
      INNER JOIN patentes pt ON p.id_patente=pt.id_patente WHERE p.ativo='S'");
  }

  public static function getPoliciaisForCombo(){

      $aData = DB::select("SELECT id_policial, nome FROM policiais WHERE ativo='S' ORDER BY nome");

      $aRet = array();
      foreach($aData as $aValue){
          $aRet[$aValue['id_policial']] = $aValue['nome'];
      }

      return $aRet;
  }

}
