<?
require_once('php/conexao.php');

if(Session::getField('id_usuario') > 0){
    Uri::RedirectTo('dashboard.php');
}

$sLoginErrorMessage = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $aDados = array(
        $_POST['username'],
        $_POST['password'],
        'S'
    );
    
    $aResult = DB::select("SELECT * FROM policiais WHERE usuario=? AND senha=? AND ativo=? LIMIT 1", $aDados);
    
    if(DB::getNumRows() > 0){
        
        Session::setUserSession($aResult[0]);
        Uri::redirectTo('dashboard.php');
        
    }else{
        
        $sLoginErrorMessage = "Usuário ou Senha inválidos!";
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Login</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Área Restrita</h1>
                    <div class="account-wall">
                        <!--<img class="profile-im" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                            alt="">-->
                        <img class="logo-dic" src="http://habbofont.com/font/paradise/DIC.gif" alt="DIC"/>
                        <form class="form-signin" method="post">
                        <input name="username" type="text" class="form-control" placeholder="Usuário" required autofocus>
                        <input name="password" type="password" class="form-control" placeholder="Senha" required>
                        
                        <?if(isset($sLoginErrorMessage)){?>
                            <p class="alert alert-danger"><?=$sLoginErrorMessage?></p>
                        <?}?>
                            
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
                        
                        <!--
                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me">
                            Remember me
                        </label>
                        <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>-->
                        </form>
                    </div>
                    <!--<a href="#" class="text-center new-account">Create an account </a>-->
                </div>
            </div>

    </body>
</html>