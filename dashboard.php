<?
require_once('php/conexao.php');
require_once('php/permissao.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Início</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    </head>
    <body>
        <div class="page-container">

            <?include("includes/header.inc.php")?>

            <div class="container">
                
                <div class="row row-offcanvas row-offcanvas-left">
                    
                    <?include("includes/sidebar.inc.php")?>
                    
                    <!-- main area -->
                    <div class="col-xs-12 col-sm-9">
                        <h1>Apresentação</h1>
                        <p>Bem vindo ao sistema DIC ... bla bla bla ...</p>
                    </div><!-- /.col-xs-12 main -->
                    
                </div><!--/.row-->
              
            </div><!--/.container-->
        </div><!--/.page-container-->
    </body>
</html>