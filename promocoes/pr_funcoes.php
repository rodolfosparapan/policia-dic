<?
function getFormDataFromDB($idPromocao){
    $aRet = DB::select("SELECT * FROM promocoes WHERE id_promocao=" . $idPromocao);
    return $aRet[0];
}

function updatePromocao($aData){

    $bSuccess = DB::update('promocoes',$aData, array('id_promocao'));

    if($bSuccess){
        Session::setFlashMessage('success', 'Promoção atualizada com sucesso!');
    }else{
        Session::setFlashMessage('danger', 'Erro ao atualizar promoção!');
    }

    return $bSuccess;
}

function insertPromocao($aData){

    $bSuccess = DB::insert('promocoes', $aData);

    if($bSuccess){
        Session::setFlashMessage('success', 'Promoção inserida com sucesso!');
        URI::redirectTo('promocoes/pr_alteracao.php?id_promocao=' . DB::getIncrementId());
    }else{
        Session::setFlashMessage('danger', 'Erro ao inserir promoção!');
    }

    return $bSuccess;
}

function deletePromocao($iIdPromocao){

    $bSuccess = DB::delete("promocoes", array("id_promocao" => $iIdPromocao));

    if($bSuccess){
        Uri::redirectTo("policiais/pl_consulta.php");
    }
    else{
        Session::setFlashMessage("danger", "Erro ao excluir Promoção!");
    }

    return $bSuccess;
}

function getPromocoesConsulta(){

    $sSql = "SELECT p.id_promocao, pl.nome AS policial, p_old.patente AS patente_antiga,
    p_new.patente AS patente_nova, p.motivo_promocao, p.dtcadastro AS dtpromocao
    FROM promocoes p
    INNER JOIN policiais pl ON p.id_policial=pl.id_policial
    INNER JOIN patentes p_old ON p.id_patente_old=p_old.id_patente
    INNER JOIN patentes p_new ON p.id_patente_new=p_new.id_patente";

    return DB::select($sSql);
}
