<?
require_once('../php/conexao.php');
require_once('../php/permissao.php');
require_once('pr_funcoes.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Promoções</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/plugins/data-table/css/jquery.bdt.css">

    </head>
    <body>
        <div class="page-container">

            <?include("../includes/header.inc.php");?>

            <div class="container">

                <div class="row row-offcanvas row-offcanvas-left">

                    <?include("../includes/sidebar.inc.php");?>

                    <!-- main area -->
                    <div class="col-xs-12 col-sm-9">
                        <h2>Promoções</h2>

                        <a href="pr_alteracao.php">
                        <button class="btn btn-success">
                            <span class="glyphicon glyphicon-plus"></span>
                            Adicionar
                        </button>
                        </a>

                        <?$aPromocoes = getPromocoesConsulta();?>
                        <?if(count($aPromocoes) > 0):?>

                            <table class="table table-hover table-striped" id="bootstrap-table">
                                <thead>
                                <tr>
                                    <th>Id Promoção</th>
                                    <th>Policial</th>
                                    <th>Antiga Patente</th>
                                    <th>Nova Patente</th>
                                    <th>Motivo Promoção</th>
                                    <th>Data Promoção</th>
                                    <th>Editar</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?foreach($aPromocoes as $aPromocao):?>
                                      <tr>
                                        <td><?=$aPromocao["id_promocao"]?></td>
                                        <td><?=$aPromocao["policial"]?></td>
                                        <td><?=$aPromocao["patente_antiga"]?></td>
                                        <td><?=$aPromocao["patente_nova"]?></td>
                                        <td><?=$aPromocao["motivo_promocao"]?></td>
                                        <td><?=Utils::showDate($aPromocao["dtpromocao"])?></td>
                                        <td><a href="pr_alteracao.php?id_promocao=<?=$aPromocao["id_promocao"]?>" title="Editar" class="glyphicon glyphicon-edit"></a></td>
                                      </tr>
                                    <?endforeach;?>
                                </tbody>
                              </table>
                          <?else:?>
                              <p style="margin-top:30px" class="alert alert-info">Não Promoções cadastradas!</p>
                          <?endif;?>


                    </div><!-- /.col-xs-12 main -->

                </div><!--/.row-->

            </div><!--/.container-->

        </div><!--/.page-container-->

        <script src="http://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="<?=Uri::getRootUri()?>assets/plugins/data-table/js/vendor/jquery.sortelements.js" type="text/javascript"></script>
        <script src="<?=Uri::getRootUri()?>assets/plugins/data-table/js/jquery.bdt.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready( function () {
                $('#bootstrap-table').bdt();
            });
        </script>

    </body>
</html>
