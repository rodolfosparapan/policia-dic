<?
require_once('../php/conexao.php');
require_once('../php/permissao.php');
require_once('../php/models/Patentes.php');
require_once('../php/models/Policiais.php');
require_once('pr_funcoes.php');

$aFillFormData = $_POST;

// Commit Data
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($_POST['id_promocao'] > 0){
        updatePromocao($_POST);
    }else{
        insertPromocao($_POST);
    }
}

// Delete data
if(!empty($_GET['_excluir'])){
    deletePromocao($_GET['id_promocao']);
}

// Retrieve Data
if(!empty($_REQUEST['id_promocao'])){
    $aFillFormData = getFormDataFromDB($_REQUEST['id_promocao']);
}

$aPoliciais = Policiais::getPoliciaisForCombo();
$aPatentes = Patentes::getPatentes();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Cadastro de Promoções</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/css/main.css">
    </head>
    <body>
        <div class="page-container">

            <?include("../includes/header.inc.php");?>

            <div class="container">

                <div class="row row-offcanvas row-offcanvas-left">

                    <?include("../includes/sidebar.inc.php");?>

                    <!-- main area -->
                    <div class="col-xs-12 col-sm-9 col-md-6">

                        <h2>Promoçoes</h2>

                        <?if(Session::hasFlashMessage()):?>
                            <p class="alert alert-<?=Session::getFlashMessageType();?>"><?=Session::getFlashMessage();?></p>
                        <?endif;?>

                        <form action="pr_alteracao.php" method="post">

                            <input type="hidden" name="id_promocao" value="0"/>

                            <div class="form-group">
                                <label>Policial</label>
                                <select name="id_policial" class="form-control" >
                                  <option value="">-Selecione um Policial</option>
                                      <?=FormHelper::getSelectOptions($aPoliciais);?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Antiga Patente</label>
                                <select name="id_patente_old" class="form-control">
                                    <option>-Selecione-</option>
                                    <?=FormHelper::getSelectOptions($aPatentes);?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nova Patente</label>
                                <select name="id_patente_new" class="form-control">
                                    <option>-Selecione-</option>
                                    <?=FormHelper::getSelectOptions($aPatentes);?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Motivo da Promoção</label>
                                <textarea name="motivo_promocao" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Treinamento + Dias (na patente)</label>
                                <textarea name="historico" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Salvar</button>
                                <a href="pr_consulta.php" class="btn btn-default">Voltar</a>
                            </div>

                        </form>



                    </div><!-- /.col-xs-12 main -->

                </div><!--/.row-->

            </div><!--/.container-->

            <?include('../includes/scripts.inc.php');?>

            <script type="text/javascript">

              var aFormData = '<?=json_encode($aFillFormData)?>';
              fillFormData(aFormData);

              function excluir(idPromocao){
                  if(confirm("Tem certeza que deseja excluir promoção?")){
                      document.location.href="pr_alteracao.php?_excluir=1&id_promocao="+idPromocao;
                  }
              }
            </script>

        </div><!--/.page-container-->
    </body>
</html>
