<?
require_once('../php/conexao.php');
require_once('../php/permissao.php');
require_once('../php/models/Policiais.php');

if(!Session::userIsAdmin())
    Uri::redirectTo();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Policiais</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/plugins/data-table/css/jquery.bdt.css">

    </head>
    <body>
        <div class="page-container">

            <?include("../includes/header.inc.php");?>

            <div class="container">

                <div class="row row-offcanvas row-offcanvas-left">

                    <?include("../includes/sidebar.inc.php");?>

                    <!-- main area -->
                    <div class="col-xs-12 col-sm-9">
                        <h2>Policiais</h2>

                        <a href="pl_alteracao.php">
                            <button class="btn btn-success">
                                <span class="glyphicon glyphicon-plus"></span>Adicionar
                            </button>
                        </a>

                        <?$aPoliciais = Policiais::getPoliciais();?>
                        <?if(count($aPoliciais) > 0):?>
                        <table class="table table-hover table-striped" id="bootstrap-table">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Login</th>
                              <th>Nome</th>
                              <th>Patente</th>
                              <th>Cadastrado em</th>
                              <th>Editar</th>
                            </tr>
                        </thead>

                        <tbody>
                          <?foreach($aPoliciais as $aPolicial):?>
                            <tr>
                                <td><?=$aPolicial['id_policial']?></td>
                                <td><?=$aPolicial['usuario']?></td>
                                <td><?=$aPolicial['nome']?></td>
                                <td><?=$aPolicial['patente']?></td>
                                <td><?=Utils::showDate($aPolicial['dtcadastro'])?></td>
                                <td><a href="pl_alteracao.php?id_policial=<?=$aPolicial["id_policial"]?>" title="Editar" class="glyphicon glyphicon-edit"></a></td>
                            </tr>
                          <?endforeach;?>
                        </tbody>
                      </table>
                      <?else:?>
                        <p style="margin-top:30px" class="alert alert-info">Não há policiais cadastrados</p>
                      <?endif;?>


                    </div><!-- /.col-xs-12 main -->

                </div><!--/.row-->

            </div><!--/.container-->

        </div><!--/.page-container-->

        <script src="http://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="<?=Uri::getRootUri()?>assets/plugins/data-table/js/vendor/jquery.sortelements.js" type="text/javascript"></script>
        <script src="<?=Uri::getRootUri()?>assets/plugins/data-table/js/jquery.bdt.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready( function () {
                $('#bootstrap-table').bdt();
            });
        </script>

    </body>
</html>
