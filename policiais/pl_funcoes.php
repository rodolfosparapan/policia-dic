<?
function validouForm($aData){

    if(!empty($aData['senha']) && $aData['senha'] != $aData['senha-confirm']){
      Session::setFlashMessage("danger", "Senhas digitadas não conferem!");
      return false;
    }

    //debug($aData);
    $isInsert = $aData['id_policial'] == null;
    if($isInsert){

        if(empty($aData['senha'])){
            Session::setFlashMessage("danger", "Informe uma Senha!");
            return false;
        }

        $policialExist = (DB::getCount("policiais", array("usuario" => $aData['usuario'], "ativo" => "S")) > 0);
        if($policialExist){
          Session::setFlashMessage("danger", sprintf("Usuário &quot%s&quot já existe!", $aData['usuario']));
          return false;
        }
    }

    return true;
}

function insert($aData){

    unset($aData['id_policial']);
    $bSuccess = DB::insert("policiais", $aData);

    if($bSuccess){
        Session::setFlashMessage("success", "Usuário inserido com sucesso!");
        Uri::redirectTo("policiais/pl_alteracao.php?id_policial=" . DB::getIncrementId());
    }
    else{
        Session::setFlashMessage("danger", "Erro ao inserir Usuário!");
    }

    return $bSuccess;
}

function update($aData){

    unset($aData['usuario']); // Para atualização, nunca altera usuário

    // Caso o usuário não informe a senha, não atualiza a mesma!
    if(empty($aData['senha']))
      unset($aData['senha']);

    $bSuccess = DB::update("policiais", $aData, array('id_policial'));

    if($bSuccess){
        Session::setFlashMessage("success", "Usuário alterado com sucesso!");
        Uri::redirectTo("policiais/pl_alteracao.php?id_policial=" . $aData['id_policial']);
    }else{
        Session::setFlashMessage("danger", "Erro ao alterar Usuário!");
    }
    return $bSuccess;
}

function delete($iIdPolicial){

    if(DB::delete("policiais", array("id_policial" => $iIdPolicial)))
        Uri::redirectTo("policiais/pl_consulta.php");
    else
        Session::setFlashMessage("danger", "Erro ao excluir Usuário!");
}

function retrieve($idPolicial){

    $aRet = DB::select("SELECT * FROM policiais WHERE id_policial=" . $idPolicial);
    unset($aRet[0]['senha']);
    return $aRet[0];
}
