<?
require_once('../php/conexao.php');
require_once('../php/permissao.php');
require_once('../php/models/Patentes.php');
require_once('pl_funcoes.php');

if(!Session::userIsAdmin())
    Uri::redirectTo();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && validouForm($_POST)){

  unset($_POST['senha-confirm']);

  if($_POST['id_policial'] > 0)
      update($_POST);
  else
      insert($_POST);
}

if(!empty($_GET['_excluir']))
    delete($_GET['id_policial']);

if(!empty($_GET['id_policial']))
    $aFillFormData = retrieve($_GET['id_policial']);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>DIC - Cadastro de Policiais</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?=Uri::getRootUri()?>assets/css/main.css">
    </head>
    <body>
        <div class="page-container">

            <?include("../includes/header.inc.php");?>

            <div class="container">

                <div class="row row-offcanvas row-offcanvas-left">

                    <?include("../includes/sidebar.inc.php");?>

                    <!-- main area -->
                    <div class="col-xs-12 col-sm-9 col-md-6">

                        <h2>Policial</h2>

                        <?if(Session::hasFlashMessage()):?>
                            <p class="alert alert-<?=Session::getFlashMessageType()?>"><?=Session::getFlashMessage()?></p>
                        <?endif;?>

                        <form action="pl_alteracao.php" method="post">

                            <input type="hidden" name="id_policial"/>

                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" required/>
                            </div>

                            <div class="form-group">
                                <label>Patente</label>
                                <select name="id_patente" class="form-control">
                                    <option>-Selecione-</option>
                                    <?
                                    $aOptions = Patentes::getPatentes();
                                    echo FormHelper::getSelectOptions($aOptions);
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                              <label>Usuário <small style="color:gray">(Usuário de login no sistema)</small></label>
                              <input type="text" name="usuario" class="form-control" required />
                            </div>

                            <div class="form-group">
                              <label>Senha</label>
                              <input type="password" name="senha" class="form-control"/>
                            </div>

                            <div class="form-group">
                              <label>Digite a senha novamente</label>
                              <input type="password" name="senha-confirm" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label>Ativo: &nbsp;</label>
                                <input type="radio" name="ativo" value="S" checked/> Sim
                                <input type="radio" name="ativo" value="N"/> Não
                            </div>

                            <div class="form-group">
                                <label>Administrador: &nbsp;</label>
                                <input type="radio" name="admin" value="S" /> Sim
                                <input type="radio" name="admin" value="N" checked/> Não
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Salvar</button>
                                <a href="pl_consulta.php" class="btn btn-default">Voltar</a>
                                <?if(!empty($aFillFormData['id_policial'])):?>
                                    <a onclick="excluir(<?=$aFillFormData['id_policial']?>)" class="btn btn-danger">Excluir</a>
                                <?endif;?>
                            </div>

                        </form>



                    </div><!-- /.col-xs-12 main -->

                </div><!--/.row-->

            </div><!--/.container-->
        </div><!--/.page-container-->

        <?include('../includes/scripts.inc.php');?>

        <script type="text/javascript">

          var aFormData = '<?=json_encode($aFillFormData)?>';
          fillFormData(aFormData);

          var idPolicial = '<?=$aFillFormData['id_policial']?>'
          if(idPolicial > 0)
              $('input[name="usuario"]').attr('disabled', 'true');

          function excluir(idPolicial){
            if(confirm("Tem certeza que deseja excluir policial?")){
              document.location.href="pl_alteracao.php?_excluir=1&id_policial="+idPolicial;
            }
          }
        </script>

    </body>
</html>
